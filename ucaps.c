#include "ucaps.h"
#include "utils.h"

/* Local Prototypes */
void SendNewKeySequence(void);
LRESULT CALLBACK LowLevelKeyboardProc(int nCode, WPARAM wParam, LPARAM lParam);

/* Global variables */
unsigned int debug_flag       = 0;
LPCSTR const MUTEX            = "ucaps-single-instance";
LPCSTR const WINDOWCLASS_NAME = "uCAPS";
HHOOK        kbdHook          = NULL;

int APIENTRY
WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    DWORD ret;
    MSG   msg;

    /* Do not start another copy  */
    CreateMutex(NULL, FALSE, MUTEX);
    ret = GetLastError();
    if ((ret == ERROR_ALREADY_EXISTS) || (ret == ERROR_ACCESS_DENIED)) {
        MessageBox(NULL, "uCaps is already running.", "uCaps", MB_OK | MB_ICONINFORMATION);
        return 1;
    }

    kbdHook = SetWindowsHookEx(WH_KEYBOARD_LL, LowLevelKeyboardProc, GetModuleHandle(NULL), 0);

    /* If GetMessage() retrieves the WM_QUIT message, the return value is zero. */
    /* TODO: May be filter messages with WM_KEYFIRST, WM_KEYLAST */
    while ((ret = GetMessage(&msg, NULL, 0, 0)) != 0) {
        if (ret == -1) {
            MessageError();
            break;
        } else {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    UnhookWindowsHookEx(kbdHook);

    return msg.wParam;
}

/*
LRESULT CALLBACK LowLevelKeyboardProc(_In_ int nCode, _In_ WPARAM wParam, _In_ LPARAM lParam);

  The hook procedure should process a message in less time than the data entry
  specified in the LowLevelHooksTimeout value in the following registry key:

  HKEY_CURRENT_USER\Control Panel\Desktop - The value is in milliseconds.

  If the hook procedure times out, the system passes the message
  to the next hook. However, on Windows 7 and later,
  the hook is silently removed without being called.
  There is no way for the application to know whether the hook is removed.

LRESULT CALLBACK KeyboardProc( _In_ int code, _In_ WPARAM wParam, _In_ LPARAM lParam);
  A code the hook procedure uses to determine how to process the message.
  If code is less than zero, the hook procedure must pass the message
  to the CallNextHookEx function without further processing and should return
  the value returned by CallNextHookEx. This parameter can be one of the following values:
HC_ACTION 0   - The wParam and lParam parameters contain information about a keystroke message.
HC_NOREMOVE 3 - The wParam and lParam parameters contain information about a keystroke message,
                and the keystroke message has not been removed from the message queue.
                (An application called the PeekMessage function, specifying the PM_NOREMOVE flag.)
*/
LRESULT CALLBACK
LowLevelKeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
    KBDLLHOOKSTRUCT *data;

    if (nCode >= 0) {

        data = (KBDLLHOOKSTRUCT*)lParam;

        debugf("wParam: 0x%08x"
                         " data->vkCode: 0x%08x"
                         " data->scanCode: 0x%08x"
                         " data->flags: 0x%08x"
                         " data->dwExtraInfo: 0x%08x",
                         wParam,
                         data->vkCode,
                         data->scanCode,
                         data->flags,
                         data->dwExtraInfo);

        /* ignore injected keystrokes */
        if ((data->flags & LLKHF_INJECTED) == 0 && data->vkCode == VK_CAPITAL) {
            if (wParam == WM_KEYUP) {
                SendNewKeySequence();
            }
            return 1;
        }
    }

    return CallNextHookEx(kbdHook, nCode, wParam, lParam);
}

/*
 UINT WINAPI SendInput(_In_ UINT nInputs, _In_ LPINPUT pInputs, _In_ int cbSize);
 RunDll32.exe shell32.dll,Control_RunDLL C:\windows\system32\input.dll
 */
void
SendNewKeySequence(void)
{
    INPUT alt_shift[4];

    ZeroMemory(alt_shift, sizeof(alt_shift));

    alt_shift[0].type = INPUT_KEYBOARD;
    alt_shift[0].ki.wVk = VK_LMENU;
    alt_shift[0].ki.wScan = 0x38u;

    alt_shift[1].type = INPUT_KEYBOARD;
    alt_shift[1].ki.wVk = VK_LSHIFT;
    alt_shift[1].ki.wScan = 0x2au;

    alt_shift[2].type = INPUT_KEYBOARD;
    alt_shift[2].ki.wVk = VK_LMENU;
    alt_shift[2].ki.wScan = 0x38u;
    alt_shift[2].ki.dwFlags = KEYEVENTF_KEYUP;

    alt_shift[3].type = INPUT_KEYBOARD;
    alt_shift[3].ki.wVk = VK_LSHIFT;
    alt_shift[3].ki.wScan = 0x2au;
    alt_shift[3].ki.dwFlags = KEYEVENTF_KEYUP;

    if(SendInput(sizeof(alt_shift)/sizeof(INPUT), alt_shift, sizeof(INPUT))
             != (sizeof(alt_shift)/sizeof(INPUT))) {
        MessageError();
    }
    return;
}

