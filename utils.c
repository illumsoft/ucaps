#include "ucaps.h"
#include <stdio.h>

#define BUFSIZE 2048

void
MessageError(void)
{
    LPTSTR errMessage;

    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER
                    | FORMAT_MESSAGE_FROM_SYSTEM
                    | FORMAT_MESSAGE_IGNORE_INSERTS,
                  NULL,
                  GetLastError(),
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                  (LPTSTR)&errMessage,
                  0,
                  NULL);

     MessageBox(NULL, errMessage, "uCaps Error", MB_OK | MB_ICONINFORMATION);
     LocalFree(errMessage);
}

void
_debugf(LPCTSTR format, ...)
{
    TCHAR buffer[BUFSIZE];
    va_list args;

    va_start(args, format);
    vsnprintf(buffer, sizeof(buffer)/sizeof(TCHAR), format, args);
    OutputDebugStringA(buffer);
}

