#ifndef _UTILS_H
#define _UTILS_H

#include "ucaps.h"

void MessageError(void);
void _debugf(LPCTSTR, ...);

#define debugf \
    if (debug_flag > 0) _debugf

#endif /* _UTILS_H */
